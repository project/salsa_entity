<?php

/**
 * @file
 * API documentation for salsa_my_donate_page.
 */

/**
 * Alter the query conditions to calculate the donation total.
 *
 * @param array $conditions
 *   Array of Salsa query conditions.
 * @param object $supporter_my_donate_page
 *   The p2p donate page.
 */
function hook_salsa_salsa_my_donate_page_donation_conditions_alter(&$conditions, $supporter_my_donate_page) {
  // Exclude donations from Jane's and John's.
  $conditions['donation.First_Name'] = array(
    '#value' => array('Jane', 'John'),
    '#operator' => 'NOT IN',
  );
}

/**
 * Alter donors before they're displayed.
 *
 * @param array $donors
 *   Array of donor arrays, keyed by the p2p page KEY.
 * @param array $supporter_my_donate_pages
 *   Array of related p2p donate pages.
 * @param array $p2p_donations
 *   Array of p2p donation information.
 */
function hook_salsa_salsa_my_donate_page_donors_alter(&$donors, $supporter_my_donate_pages, $p2p_donations) {
  // Ignore payments below 20.
  foreach ($donors as $supporter_my_donate_page_KEY => $donors_page) {
    foreach ($donors_page as $key => $donor) {
      if ($donor['amount'] < 20) {
        unset($donors[$supporter_my_donate_page_KEY][$key]);
      }
    }
  }
}
