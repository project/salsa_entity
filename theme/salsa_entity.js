/**
 * @file
 * JS Behaviors for the salsa entity progress bar.
 */

(function($) {
  Drupal.behaviors.salsaEntityProgressBar = {
    attach: function(context, settings) {

      var progress_conf = Drupal.settings.salsaEntityProgressBar;
      var stack = [];

      // Make sure the progress bar animation runs only once. Without this, it
      // will re-run upon re-attaching of behaviors after AJAX request.
      $('body').once('progress-bar', function () {
        // Progress bar might be disabled and settings unavailable.
        if (progress_conf !== undefined) {
          for (var i = 0; i < progress_conf.length; i++) {
            // Only add data for animation if there was any progress.
            if (progress_conf[i]['total'] > 0) {
              var config = progress_conf[i];
              var $wrapper = $('.salsa-entity-' + config.entity_id);

              // Get the data for each progress bar and push it on the stack.
              stack[i] = {
                config: progress_conf[i],
                $wrapper: $wrapper,
                $progress_bar:  $wrapper.find('.salsa-barometer'),
                $progress_indicator: $wrapper.find('.salsa-barometer-progress'),
                // Ensure the animation won't take too long.
                step: config.percent_label ? 1 : config.goal/100,
                complete: false,
                value: 0
              };
            }
          }

          // Start the animation.
          setTimeout(progress, progress_conf[0].animation_speed);
        }
      });

      /**
       * Increases value and width of the progress bar.
       *
       * Calls itself after some timeout until the maximum value is reached.
       */
      function progress() {
        var all_complete = true;
        for (var i = 0; i < stack.length; i++) {
          // The total is either a percent amount of the goal or the passed
          // total if label is counted up in absolute parts.
          var total =  stack[i].config.percent_label ? parseInt(stack[i].config.total/stack[i].config.goal * 100) : stack[i].config.total;
          if (!stack[i].complete) {
            // Increase the value for 1% point and store it on the stack.
            stack[i].value += stack[i].step;
            // Also increase the width of the progress bar.
            stack[i].$progress_indicator.css('width', Math.min(100, parseInt(stack[i].value/stack[i].step)) + '%');
            all_complete = false;
          }

          // Loop until the total is reached or the value exceeds 100%.
          if (!stack[i].complete && (stack[i].value >= total || parseInt(stack[i].value/stack[i].step) >= 100)) {
            // Mark current progress bar as done.
            stack[i].complete = true;
          }
        }

        if (!all_complete) {
          // Loop.
          setTimeout(progress, stack[0].config.animation_speed);
        }
      }

    }
  }
}(jQuery));
