<?php

/**
 * @file
 * Salsa Entity test classes.
 */

/**
 * Base class for salsa entity tests.
 */
class SalsaEntityBaseTestCase extends DrupalWebTestCase {

  public $profile = 'testing';

  protected $salsaAPIClass = 'SalsaAPIMock';

  public function setUp() {
    $modules = func_get_args();
    if (isset($modules[0]) && is_array($modules[0])) {
      $modules = $modules[0];
    }
    $modules = array_merge(array('salsa_entity'), $modules);
    parent::setUp($modules);

    variable_set('salsa_api_class', $this->salsaAPIClass);

    entity_info_cache_clear();
    entity_property_info_cache_clear();
    menu_rebuild();
    // Refresh permissions and clear caches.
    $this->checkPermissions(array(), TRUE);
  }
}

/**
 * Tests basic API
 */
class SalsaEntityAPITestCase extends SalsaEntityBaseTestCase {

  public static function getInfo() {
    return array(
      'name' => t('Get Supporter'),
      'description' => t('Check if the supporter data loaded correctly.'),
      'group' => t('Salsa API'),
    );
  }

  /**
   * Check if the supporter data loaded correctly.
   */
  function testGetSupporter() {
    // Request for a user that exists.
    $supporter = entity_load_single('salsa_supporter', '1');
    $this->assertTrue(is_object($supporter), t('Supporter is loaded successfully.'));
    $this->assertEqual($supporter->First_Name, 'First Name', t('Supporter name is: @name', array('@name' => 'First Name')));
    $this->assertEqual($supporter->Email, 'example@example.com', t('Supporter Email is: @email', array('@email' => 'example@example.com')));

    // Request for users that do not exist.
    $supporter = entity_load_single('salsa_supporter', '00000002');
    $this->assertFalse($supporter, t('Supporter with supporter_KEY @key does not exist.', array('@key' => '00000002')));
  }
}

/**
 * Tests basic API for translations.
 */
class SalsaEntityTranslationTestCase extends SalsaEntityBaseTestCase {

  public static function getInfo() {
    return array(
      'name' => 'Salsa translations',
      'description' => 'Check if the translations made in Salsa are loaded and processed correctly.',
      'group' => 'Salsa API',
    );
  }

  public function setUp() {
    parent::setUp(array('salsa_event', 'locale', 'salsa_entity'));
  }

  /**
   * Check for translation support on SalsaEntity.
   *
   * We mock the following values on Event Nr. 1:
   *  de: Description: Description 1 german
   *  de: Directions:  Directions 1 german
   *  fr: Event_Name:  Event_Name 1 french
   */
  function testGetTranslationGerman() {
    $event = entity_load_single('salsa_event', '1');
    $this->assertTrue(is_object($event), 'Event for translation is loaded successfully.');

    $this->assertEqual($event->getTranslation('Description', 'de'), 'Description 1 german');
    $this->assertEqual($event->getTranslation('Event_Name',  'de'), 'Test Event');
    $this->assertEqual($event->getTranslation('Directions',  'de'), 'Directions 1 german');

    $this->assertEqual($event->getTranslation('Description', 'fr'), '<p>&#160;</p>');
    $this->assertEqual($event->getTranslation('Event_Name',  'fr'), 'Event_Name 1 french');
    $this->assertEqual($event->getTranslation('Directions',  'fr'), NULL);

    // Create a different event, make sure that translations are not applied
    // for that.
    $event2 = entity_create('salsa_event', array('Event_Name' => $this->randomName(), 'event_KEY' => 2));
    $this->assertEqual($event2->getTranslation('Event_Name',  'fr'), $event2->Event_Name);
    $this->assertEqual($event2->getTranslation('Event_Name',  'de'), $event2->Event_Name);
  }

  /**
   * Check whether language is saved proper on supporter objects.
   */
  function testSupporterLanguageStorage() {
    global $language;
    $language = (object) array(
      'language' => 'de',
      'name' => 'German',
      'native' => 'Deutsch',
      'direction' => 0,
      'enabled' => 1,
      'plurals' => 0,
      'formula' => '',
      'domain' => '',
      'prefix' => '',
      'weight' => 0,
      'javascript' => '',
    );
    $supporter_id = 1;

    // Disable language storage on supporter objects and make sure the language
    // code isn't saved.
    variable_set('salsa_entity_set_supporter_language', FALSE);
    $supporter = entity_load_single('salsa_supporter', $supporter_id);
    $supporter->save();
    $mock = variable_get('salsa_entity_mock_saved');
    $this->assertTrue(empty($mock['supporter'][$supporter_id]['Language_Code']));

    // Now enable language storage on supporter objects and test if the language
    // code gets saved.
    variable_set('salsa_entity_set_supporter_language', TRUE);
    $supporter = entity_load_single('salsa_supporter', $supporter_id);
    $supporter->save();
    $mock = variable_get('salsa_entity_mock_saved');
    $this->assertEqual($mock['supporter'][$supporter_id]['Language_Code'], SalsaEntityLanguageMapper::toIso639_2($language->language));

    // Load a supporter that already has a language code set to "eng" and test
    // that the language code doesn't get overridden with "ger", the current
    // language.
    $supporter_id = 2;
    $supporter = entity_load_single('salsa_supporter', $supporter_id);
    $supporter->save();
    $mock = variable_get('salsa_entity_mock_saved');
    $this->assertEqual($supporter->Language_Code, $mock['supporter'][$supporter_id]['Language_Code']);
  }

}
