<?php

/**
 * @file
 */

/**
 * Implements hook_salsa_object_type_info().
 */
function salsa_questionnaire_salsa_object_type_info() {
  return array(
    'questionnaire' => array(
      'label' => t('Salsa Questionnaire'),
      'entity class' => 'SalsaEntityQuestionnaire',
      'entity keys' => array(
        'label' => 'Reference_Name',
      ),
      'salsa_cache' => 600,
    ),
    'questionnaire_page' => array(
      'label' => t('Salsa Questionnaire Page'),
      'salsa_cache' => 600,
    ),
    'questionnaire_question' => array(
      'label' => t('Salsa Questionnaire Question'),
    ),
    'questionnaire_question_option' => array(
      'label' => t('Salsa Questionnaire Question Option'),
    ),
    'supporter_questionnaire_question' => array(
      'label' => t('Salsa Supporter Questionnaire Question'),
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function salsa_questionnaire_block_info() {
  $blocks = array();
  if (!salsa_entity_is_configured()) {
    return $blocks;
  }

  $questionnaires = entity_load('salsa_questionnaire');
  if (is_array($questionnaires)) {
    foreach ($questionnaires as $questionnaire) {
      $blocks[$questionnaire->questionnaire_KEY] = array(
        'info' => $questionnaire->Reference_Name,
        'cache' => DRUPAL_NO_CACHE,
      );
    }
  }

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function salsa_questionnaire_block_view($delta = '') {
  $block = array();
  $questionnaire_page = entity_load_single('salsa_questionnaire', $delta);

  $block['subject'] = is_object($questionnaire_page) ? $questionnaire_page->Reference_Name : t('Error loading page');
  $block['content'] = is_object($questionnaire_page) ? $questionnaire_page->buildContent() : t('Content is currently not available. Please click !reload_link to refresh the page.', array('!reload_link' => l(t('here'), current_path())));

  return $block;
}

/**
 * Questionnaire form.
 */
function salsa_questionnaire_form($form, &$form_state, $questionnaire) {
  $form_state['salsa_object'] = $questionnaire;
  $form_state['questions'] = entity_load('salsa_questionnaire_question', FALSE, array('questionnaire_KEY' => $questionnaire->questionnaire_KEY));
  $form_state['supporter'] = salsa_entity_get_supporter();

  // Build supporter fieldset.
  salsa_entity_supporter_fieldset($form, $form_state, 'salsa_questionnaire_form');

  // Add the checkboxes for the optional groups, if available.
  $optionally_add_to_groups_KEYS = salsa_entity_string_explode($questionnaire->optionally_add_to_groups_KEYS);
  if (!empty($optionally_add_to_groups_KEYS)) {
    salsa_entity_add_optional_groups_checkboxes(
      $form['Supporter_Info'],
      $optionally_add_to_groups_KEYS,
      $form_state['supporter'],
      'optionally_add_to_groups_KEYS'
    );
  }

  // Questionnaire fieldset.
  $form['Questions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Questions'),
  );

  $responses = array();
  if (isset($form_state['supporter']->supporter_KEY)) {
    $responses = entity_load('salsa_supporter_questionnaire_question', FALSE, array('supporter_KEY' => $form_state['supporter']->supporter_KEY));
  }
  $question_KEYS = array_keys($form_state['questions']);
  foreach ($responses as $key => $response) {
    if (!in_array($response->questionnaire_question_KEY, $question_KEYS)) {
      unset($responses[$key]);
    }
  }
  if (!empty($responses)) {
    drupal_set_message(t('You have already completed this questionnaire, but you may update your answers if you wish.'), 'status', FALSE);
  }
  foreach ($form_state['questions'] as $question) {
    salsa_questionnaire_build_question_field($form, $question, $responses);
  }

  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Questionnaire form validate.
 */
function salsa_questionnaire_form_validate($form, &$form_state) {
  if (!valid_email_address($form_state['values']['Email'])) {
    form_set_error('Email', t('Please enter a valid E-mail address.'));
  }
}

/**
 * Questionnaire form submit.
 */
function salsa_questionnaire_form_submit($form, &$form_state) {
  // Create a new one or update existing supporter.
  salsa_entity_supporter_fieldset_submit($form, $form_state);

  // First delete all supporter groups.
  salsa_entity_delete_supporter_groups($form_state['salsa_object']->add_to_groups_KEYS, $form_state['salsa_object']->optionally_add_to_groups_KEYS);

  // Then add supporter to the groups if any specified.
  $questionnaire = $form_state['salsa_object'];
  $groups_KEYS = salsa_entity_string_explode($questionnaire->add_to_groups_KEYS);
  if (!empty($form_state['values']['optionally_add_to_groups_KEYS'])) {
    $optionally_add_to_groups_KEYS = array_filter($form_state['values']['optionally_add_to_groups_KEYS']);
    $groups_KEYS = array_unique(array_merge($groups_KEYS, $optionally_add_to_groups_KEYS));
  }
  if (!empty($groups_KEYS)) {
    foreach ($groups_KEYS as $key) {
      $supporter_groups[$key] = new SalsaEntity(array(), 'salsa_supporter_groups');
      $supporter_groups[$key]->organization_KEY = $questionnaire->organization_KEY;
      $supporter_groups[$key]->supporter_KEY = $form_state['supporter']->supporter_KEY;
      $supporter_groups[$key]->groups_KEY = $key;
      $supporter_groups[$key]->save();
    }
  }

  // Save answers.
  $index = 1;
  foreach ($form_state['questions'] as $question) {
    $questionnaire_response[$index] = new SalsaEntity(array(), 'salsa_supporter_questionnaire_question');
    $questionnaire_response[$index]->organization_KEY = $question->organization_KEY;
    $questionnaire_response[$index]->supporter_KEY = $form_state['supporter']->supporter_KEY;
    $questionnaire_response[$index]->questionnaire_question_KEY = $question->questionnaire_question_KEY;

    if (is_array($form_state['values']['Response_Question_' . $question->questionnaire_question_KEY])) {
      $questionnaire_response[$index]->Response = implode(',', array_filter($form_state['values']['Response_Question_' . $question->questionnaire_question_KEY]));
    }
    else {
      $questionnaire_response[$index]->Response = $form_state['values']['Response_Question_' . $question->questionnaire_question_KEY];
    }

    $questionnaire_response[$index]->save();
    $index++;
  }

  // Redirect user if path is set.
  if ($questionnaire->Show_Results == 'true') {
    // @todo Show questionnaire results.
  }
  elseif ($questionnaire->redirect_path) {
    $form_state['redirect'] = $questionnaire->redirect_path;
  }
}

/**
 * Helper function that builds question fields used on questionnaire form.
 *
 * @param array $form
 *   Drupal Form.
 * @param array $question
 *   Salsa Questions.
 * @param array $responses
 *   Salsa Supporter Responses.
 */
function salsa_questionnaire_build_question_field(&$form, $question, $responses) {
  // SetUp basic field properties.
  $form['Questions']['Response_Question_' . $question->questionnaire_question_KEY] = array(
    '#title' => check_plain($question->getTranslation('Question')),
    '#prefix' => '<div class="qHeader">' . $question->getTranslation('Header') . '</div>',
    '#suffix' => '<div class="qFooter">' . $question->getTranslation('Footer') . '</div>',
    '#required' => $question->This_Question_Is_Required == 'true' ? TRUE : FALSE,
  );

  // SetUp $options array.
  $options = array();
  if (!empty($question->PRIVATE_Options)) {
    $PRIVATE_Options = explode('|', $question->PRIVATE_Options);
    foreach ($PRIVATE_Options as $option) {
      $item = explode('~', substr($option, 0, -3));
      $options[$item[1]] = $item[0];
    }
  }

  // SetUp default values (answers).
  foreach ($responses as $response) {
    if ($response->questionnaire_question_KEY == $question->questionnaire_question_KEY) {
      if ($question->Response_Type == 'Multiple Choice (Checkbox)') {
        $form['Questions']['Response_Question_' . $question->questionnaire_question_KEY] += array('#default_value' => explode(',', $response->Response));
      }
      else {
        $form['Questions']['Response_Question_' . $question->questionnaire_question_KEY] += array('#default_value' => $response->Response);
      }
    }
  }

  // Specific field properties.
  switch ($question->Response_Type) {
    case 'Text':
    case 'Number':
      $form['Questions']['Response_Question_' . $question->questionnaire_question_KEY] += array(
        '#type' => 'textfield',
      );
      break;

    case 'Paragraph':
      $form['Questions']['Response_Question_' . $question->questionnaire_question_KEY] += array(
        '#type' => 'textarea',
      );
      break;

    case 'Multiple Choice (Dropdown)':
      $form['Questions']['Response_Question_' . $question->questionnaire_question_KEY] += array(
        '#type' => 'select',
        '#options' => $options,
      );
      break;

    case 'Multiple Choice (Radio)':
      $form['Questions']['Response_Question_' . $question->questionnaire_question_KEY] += array(
        '#type' => 'radios',
        '#options' => $options,
      );
      break;

    case 'Multiple Choice (Checkbox)':
      $form['Questions']['Response_Question_' . $question->questionnaire_question_KEY] += array(
        '#type' => 'checkboxes',
        '#options' => $options,
      );
      break;
  }
}
