<?php

/**
 * @file
 * Metadata controllers for the Salsa entity types.
 */


/**
 * Default Salsa Metadata controller.
 */
class SalsaEntityMetadataController extends EntityDefaultMetadataController {

  static $customColumns;

  /**
   * The salsa type.
   *
   * @var string
   */
  public $salsaType;

  public function __construct($entityType) {
    parent::__construct($entityType);
    $this->salsaType = str_replace('salsa_', '', $entityType);
  }

  /**
   * Overrides EntityDefaultMetadataController::entityPropertyInfo().
   */
  public function entityPropertyInfo() {
   // @todo: Add a is connected check to SalsaAPIInterface.
   try {
     return $this->buildEntityPropertyInfo();
   } catch (SalsaQueryException $e) {
     watchdog_exception('salsa_entity', $e);
   }
   return array();
  }

  /**
   * Builds the entity property information based on information from Salsa.
   *
   * @return array
   *   Entity property info.
   */
  protected function buildEntityPropertyInfo() {
    $salsa_info = $this->getSalsaInfo();

    $info = array();
    if (is_array($salsa_info)) {
      foreach ($salsa_info as $item) {
        // Base info
        $label = isset($item['displayName']) ? $item['displayName'] : $item['label'];
        $info[$this->type]['properties'][$item['name']] = array(
          'label' => t($label),
          'type' => $this->_entity_metadata_convert_schema_type($item['type']),
          'salsa_type' => $item['type'],
        );

        // Reference linked linked entities.
        if (strpos($item['name'], '_KEY') !== FALSE) {
          $target_type = substr($item['name'], 0, strpos($item['name'], '_KEY'));
          $info[$this->type]['properties'][$item['name']]['type'] = 'salsa_' . $target_type;
        }

        // If $item is type of enum or set, use callback method to get options.
        if ((strpos($item['type'], 'enum') === 0 || strpos($item['type'], 'set') === 0) && !empty($item['values'])) {
          $info[$this->type]['properties'][$item['name']]['options list'] = 'salsa_entity_property_info_options';
          if (!empty($item['values'])) {
            foreach (explode(',', $item['values']) as $value) {
              $info[$this->type]['properties'][$item['name']]['options'][$value] = t($value);
            }
          }
        }

        // If $item is a custom column, get the options.
        foreach ($this->getCustomColumns() as $column) {
          // Ignore columns without a name.
          if (empty($column->name)) {
            continue;
          }

          if ($item['name'] == $column->name) {
            $info[$this->type]['properties'][$item['name']]['#salsa_custom'] = TRUE;
            if ($item['type'] == 'enum' && !empty($column->options)) {
              $info[$this->type]['properties'][$item['name']]['options'] = array();
              foreach ($column->options as $option) {
                $info[$this->type]['properties'][$item['name']]['options'][$option->value] = t($option->label);
                if ($option->isDefault_BOOLVALUE === TRUE) {
                  $info[$this->type]['properties'][$item['name']]['default_value'] = $option->value;
                }
              }
            }
          }
        }
      }
      // Add the url property.
      $info[$this->type]['properties']['url'] = array(
        'label' => t("URL"),
        'description' => t("The URL of the entity."),
        'getter callback' => 'entity_metadata_entity_get_properties',
        'type' => 'uri',
        'computed' => TRUE,
      );
    }

    return $info;
  }

  /**
   * Overrides EntityDefaultMetadataController::_entity_metadata_convert_schema_type().
   */
  public function _entity_metadata_convert_schema_type($type) {
    // @todo: Do we need this configurable?
    $types_map = array(
      'text' => array(
        'varchar',
        'text',
        'blob',
        'enum',
        'set',
      ),
      'integer' => array(
        'int',
        'tinyint',
      ),
      'decimal' => array(
        'float',
        'double',
        'decimal',
      ),
      'date' => array(
        'datetime',
        'date',
        'timestamp',
      ),
      'boolean' => array(
        'bool',
      ),
    );

    // Find the right entity type for submitted salsa type.
    foreach ($types_map as $entity_type => $salsa_types) {
      foreach ($salsa_types as $findme) {
        if (strpos($type, $findme) !== FALSE) {
          return $entity_type;
        }
      }
    }

    // Default type
    return 'text';
  }

  /**
   * Method that loads custom fields.
   */
  private function getCustomColumns() {
    if (self::$customColumns === NULL) {
      $cid = 'custom_column';
      if (!(self::$customColumns = salsa_entity_load_cache_item($cid))) {

        $api = salsa_api();
        self::$customColumns = $api->getObjects('custom_column');

        if (!empty(self::$customColumns)) {

          $offset = 0;

          do {
            $options = $api->getObjects('custom_column_option', array(), "$offset,500");
            foreach ($options as $option) {
              if ($option->isDisplayed == 'true') {
                if (intval($option->_Order) < 0) {
                  $option->isDefault = 'true';
                  $option->isDefault_BOOLVALUE = TRUE;
                }
                else {
                  $option->isDefault = 'false';
                  $option->isDefault_BOOLVALUE = FALSE;
                }
                self::$customColumns[$option->custom_column_KEY]->options[$option->custom_column_option_KEY] = $option;
              }
            }
            $offset += 500;
          } while (count($options) === 500);

          // Sort all options based on the order defined in salsa.
          foreach (self::$customColumns as &$field) {
            if (property_exists($field, 'options') && is_array($field->options)) {
              uasort($field->options, array('SalsaEntityMetadataController', '_compare_custom_options'));
            }
          }

          // Cache all custom columns.
          salsa_entity_save_cache_item($cid, self::$customColumns);
        }
      }
    }

    return self::$customColumns;
  }

  /**
   * Compares two salsa options from a custom field for use in uasort().
   *
   * @param stdClass $a
   *   First salsa option object to compare.
   * @param stdClass $b
   *   Second salsa option object to compare.
   *
   * @return int
   *   If the first argument is less, equal or greater than the second.
   */
  public static function _compare_custom_options($a, $b) {
    // object->_Order (object->isDefault?)
    if (($a instanceof stdClass) && ($b instanceof stdClass) && property_exists($a, '_Order') && property_exists($b, '_Order')) {
      return ($a->_Order > $b->_Order) ? 1 : (($a->_Order < $b->_Order) ? -1 : 0);
    }
    return 0;
  }

  /**
   * Returns the Salsa information about the current salsa type.
   *
   * @return array
   *  The salsa information.
   *
   * @throws \Exception
   * @throws \SalsaQueryException
   */
  protected function getSalsaInfo() {
    $cid = 'describe2:' . $this->salsaType;

    // Return cached information if available.
    if ($salsa_info = salsa_entity_load_cache_item($cid)) {
      return $salsa_info;
    }

    // If not, add a lock to prevent write conflicts with multiple requests.
    if (lock_acquire($cid)) {
      if ($salsa_info = salsa_api()->describe2($this->salsaType)) {
        salsa_entity_save_cache_item($cid, $salsa_info);
      }
      lock_release($cid);

      return $salsa_info;
    }
    else {
      // If we failed to acquire the lock, wait as the entity information is
      // otherwise incomplete and try again.
      lock_wait($cid);
      return $this->getSalsaInfo();
    }
  }
}
