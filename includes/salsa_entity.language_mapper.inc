<?php

/**
 * @file
 * Contains SalsaEntityLanguageMapper.
 */

/**
 * Static helper class.
 *
 * Gets the ISO-639-2 language code from a given ISO-639-1 and vice versa.
 */
class SalsaEntityLanguageMapper {

  /**
   * Mapping of language codes, keyed by ISO-639-1.
   *
   * @var array
   */
  protected static $code_mapping = array(
    'de' => 'ger',
    'en' => 'eng',
    'fr' => 'fre',
    'it' => 'ita',
    'es' => 'spa',
    'ru' => 'rus',
    'ca' => 'cat',
    'ar' => 'ara',
    'ku' => 'kur',
    'pt' => 'por',
  );

  /**
   * Returns a ISO-639-2 code for the requested language.
   *
   * @param string $langcode
   *   (optional) ISO-639-1 Code language - 2-char, defaults to the current
   *   language.
   *
   * @return string|null
   *   The ISO-639-2 or NULL if the language is not defined yet.
   */
  public static function toIso639_2($langcode = NULL) {
    global $language;

    if (!isset($langcode)) {
      $langcode = $language->language;
    }
    // Provide possibility to override code mapping.
    $codes = variable_get('salsa_entity_supporter_language_code_mapping', self::$code_mapping);

    if (isset($codes[$langcode])) {
      return $codes[$langcode];
    }
    return NULL;
  }

  /**
   * Returns a ISO-639-1 code for the requested language.
   *
   * @param string $langcode
   *   ISO-639-2 Code language - 3-char.
   *
   * @return string|null
   *   The ISO-639-1 or NULL if the language is not defined yet.
   */
  public static function fromIso639_2($langcode) {
    // Provide possibility to override code mapping.
    $codes = variable_get('salsa_entity_supporter_language_code_mapping', self::$code_mapping);
    if ($key = array_search($langcode, $codes)) {
      return $codes[$key];
    }
    return NULL;
  }

}
