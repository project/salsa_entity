<?php

/**
 * @file
 * Definition of SalsaEntity.
 */

/*
 * Entity class for the Salsa entity.
 */
class SalsaEntity extends Entity {

  /**
   * Can be used to pass additional fields to the save callback.
   *
   * @var array
   */
  public $additional = array();

  protected static $translations = array();

  /**
   * Overrides Entity::buildContent().
   */
  public function buildContent($view_mode = 'full', $langcode = NULL) {
    $wrapper = entity_metadata_wrapper($this->entityType, $this);
    foreach ($wrapper as $key => $property) {
      $info = $property->info();
      $content[$key] = array(
        '#type' => 'item',
        '#title' => $info['label'],
        '#markup' => $property->value(),
      );
    }
    return entity_get_controller($this->entityType)->buildContent($this, $view_mode, $langcode, $content);
  }

  public function defaultUri() {
    return array(
      'path' => 'salsa/' . $this->getSalsaType() . '/' . $this->{$this->idKey},
      'options' => array(),
    );
  }

  /**
   * Get the entity type without the "salsa_" prefix.
   *
   * @return string
   */
  protected function getSalsaType() {
    return str_replace('salsa_', '', $this->entityType);
  }

  /**
   * Defines the entity label if the 'entity_class_label' callback is used.
   */
  protected function defaultLabel() {
    if (!empty($this->entityInfo['entity keys']['label'])) {
      return $this->getTranslation($this->entityInfo['entity keys']['label']);
    }
  }

  /**
   * Getter for the related tags of a salsa entity.
   */
  public function getTags() {
    // @todo unfortunately using getLeftJoin doesn't work for some reason yet.
    // $result = salsa_api()->getLeftJoin(array('tag', 'tag_data', 'database_table'), array('table_name' =>  str_replace('salsa_', '', $this->entityType), 'table_key' =>  $this->{$this->idKey}), NULL, array('tag_KEY', 'tag'));

    // If object was not saved yet return.
    if (empty($this->{$this->idKey})) {
      return array();
    }

    // Load related tag objects.
    $entities = entity_load('salsa_database_table', FALSE, array('table_name' => str_replace('salsa_', '', $this->entityType)));
    if (empty($entities)) {
      return array();
    }
    $database_table = reset($entities);
    $tag_data = entity_load('salsa_tag_data', FALSE, array(
      'database_table_KEY' => $database_table->database_table_KEY,
      'table_key' => $this->{$this->idKey},
    ));

    $return = array();
    // Get the tag keys.
    foreach ($tag_data as $tag_data_key => $tag_data) {
      $tag_keys[] = $tag_data->tag_KEY;
    }
    if (!empty($tag_keys)) {
      // Load the tag labels.
      $tags = entity_load('salsa_tag', $tag_keys);
      foreach ($tags as $delta => $tag) {
        $return[$tag->tag_KEY] = $tag->tag;
      }
    }

    return $return;
  }

  /**
   * Implements Entity::getTranslation().
   */
  public function getTranslation($property, $langcode = NULL) {
    // The language code from salsa is in ISO-639-2, drupal language is in
    // ISO-639-1 - this means we need to convert the 2-char drupal language
    // code into a 3-char salsa language code.
    $iso639_2_langcode = SalsaEntityLanguageMapper::toIso639_2($langcode);

    if (!isset(self::$translations[$iso639_2_langcode][$this->getSalsaType()][$this->identifier()])) {
      self::$translations[$iso639_2_langcode][$this->getSalsaType()][$this->identifier()] = $this->loadTranslation($iso639_2_langcode);
    }
    $data = self::$translations[$iso639_2_langcode][$this->getSalsaType()][$this->identifier()];

    // Return the translated value or the default one if there is no translation.
    if (isset($data[$property])) {
      return $data[$property];
    }

    // Return the default value or an empty string.
    return isset($this->{$property}) ? $this->{$property} : NULL;
  }

  /**
   * Loads the translation data for the current entity and given language.
   *
   * @param string $iso639_2
   *   Language code.
   *
   * @return array
   *   Array with translations.
   */
  protected function loadTranslation($iso639_2) {
    // Check for cached data.
    $cache_key = $iso639_2 . ':' . $this->getSalsaType() . ':' . $this->identifier();
    $cache = cache_get($cache_key, 'cache_salsa_translation');

    if ($cache && (REQUEST_TIME < $cache->expire)) {
      return $cache->data;
    }
    else {
      $data = array();
      if ($salsa = salsa_api()) {
        // Build the term based on the object and the ID of this entity.
        $term = $this->getSalsaType() . ',' . $this->identifier() . ',%';

        // build the conditions array and get all values form salsa.
        $conditions = array(
          'Status' => array('#value' => 'Active', '#operator' => '='),
          'language_code' => array('#value' => $iso639_2, '#operator' => '='),
          'term' => array('#value' => $term, '#operator' => 'LIKE'),
        );
        $values = $salsa->getObjects('translation', $conditions);

        // Get all translations and cache the results.
        foreach ($values as $item) {
          if ($item->language_code == $iso639_2) {
            $field = substr($item->term, strrpos($item->term, ',') + 1);
            $data[$field] = $item->translation;
          }
        }

        // Cache the result for 10 minutes.
        cache_set($cache_key, $data, 'cache_salsa_translation', REQUEST_TIME + 600);
      }
      return $data;
    }
  }
}
