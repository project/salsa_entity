<?php

/**
 * @file
 * Hooks provided by the Salsa Entity module.
 */


/**
 * Provide information about salsa object types.
 */
function hook_salsa_object_type_info() {
  return array(
    'supporter' => array(
      'label' => t('Salsa Supporter'),
    ),
    'supporter_groups' => array(
      'label' => t('Salsa Supporter Groups'),
    ),
    'supporter_event' => array(
      'label' => t('Salsa Supporter Event'),
    ),
    'event' => array(
      'label' => t('Salsa Event'),
    ),
    'distributed_event' => array(
      'label' => t('Salsa Distributed Event'),
    ),
    'donation' => array(
      'label' => t('Salsa Donation'),
    ),
    'groups' => array(
      'label' => t('Salsa Groups'),
    ),
    'donate_page' => array(
      'label' => t('Donate Page'),
      'entity class' => 'SalsaEntityDonatePage',
    ),
  );
}

/**
 * Alter salsa object types information.
 *
 * @param $info
 *   The defined salsa object types information.
 *
 * @see hook_salsa_object_type_info()
 */
function hook_salsa_object_type_info_alter(&$info) {
  $info['supporter']['label'] = t('Updated Salsa Supporter');
  $info['supporter_groups']['label'] = t('Updated Salsa Supporter Groups');
  $info['supporter_event']['label'] = t('Updated Salsa Supporter Eventr');
  $info['event']['label'] = t('Updated Salsa Event');
  $info['distributed_event']['label'] = t('Updated Salsa Distributed Event');
  $info['donation']['label'] = t('Updated Salsa Donation');
  $info['groups']['label'] = t('Updated Salsa Groups');
  $info['donate_page']['label'] = t('Updated Donate Page');
}

/**
 * Hook that allows other modules to suggest a current supporter.
 *
 * @param string $mail
 *   E-mail address
 * @return object $supporter
 *   Salsa Supporter
 */
function hook_salsa_entity_current_supporter($mail = NULL) {
  if ($mail == 'example@example.com') {
    return reset(entity_load('salsa_supporter', FALSE, array('Email' => $mail)));
  }
}

/**
 * Hook that allows other modules to set supporter if user is logged in.
 *
 * @param object $supporter
 *   Salsa Supporter
 */
function hook_salsa_entity_supporter_set($supporter) {
  salsa_profile_set_supporter($supporter);
}

/**
 * Allow other modules to alter supporter fieldset.
 *
 * @param $form
 *   The form array.
 * @param $form_state
 *   The  standard associative array containing the current state of the form.
 * @param $form_id
 *   The form ID to find the callback for.
 * @return
 *   Form array.
 */
function hook_salsa_entity_supporter_fieldset_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'example_form') {
    // Add gender.
    $form['gender'] = array(
      '#type' => 'select',
      '#title' => t('Gender'),
      '#options' => array(
        'M' => t('Male'),
        'F' => t('Female'),
      ),
    );
  }
}

/**
 * Alter the formatted currency amount.
 *
 * @param float &$amount
 *   The currency amount as a float.
 */
function hook_salsa_amount_alter(&$amount) {
  $amount .= ' USD';
}

/**
 * Alter a Salsa-Entity field query based on tags.
 *
 * Tags can be added on the EntityFieldQuery with:
 *  $query->addTag('TAG');
 *
 * @param array $conditions
 *   Associative array with all currently defined conditions:
 *   - field_name: Associative conditions-array.
 *     - #value: Field value.
 *     - #operator: Operator to use for the condition.
 *   Can additionally contain the special keys #order and #limit, which can be
 *   altered too.
 * @param EntityFieldQuery $query
 *   The current Entity-Field query. Only provided for context, changes have no
 *   effect.
 */
function hook_salsa_query_TAG_alter(array &$conditions, EntityFieldQuery $query) {
  // Add a new field as a condition.
  $conditions['field_name'] = array(
    '#value' => 'some value',
    '#operator' => '=',
  );

  // Define a different order field, prepend - for descending order.
  $conditions['#order'] = array('-field_name_2');
}

/**
 * Alter the labels that are displayed within the progress bar.
 *
 * @param int $goal
 *   The number representing the total width of the progress bar.
 * @param int $form_id
 *   The form id (form function name) of the form that adds the progress bar.
 * @param int $entity_id
 *   The entity id of the salsa form that is requesting the progress bar.
 */
function hook_salsa_entity_progress_bar_labels_alter(&$labels, $form_id, $entity_id) {
  if ($form_id == 'salsa_donate_page_form') {
    // Check which entities use percentage display.
    $percent_styles = variable_get('salsa_entity_progress_bar_animation_style_percent', array());
    // Change the labels.
    $labels['goal'] = salsa_entity_get_amount($labels['goal'], TRUE);
    // Don't override the value label if percentage display was selected.
    $labels['total'] = in_array($entity_id, $percent_styles) ? salsa_entity_get_amount($labels['total'], TRUE) : $labels['total'];
    $labels['goal_label'] = t('Donation goal:');
    $labels['total_label'] = t('Donated:');
  }
}
