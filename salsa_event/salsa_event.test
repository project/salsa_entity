<?php

/**
 * @file
 * Salsa commerce integration tests.
 */

class SalsaEventTestCase extends SalsaEntityBaseTestCase {

  public function setUp() {
    parent::setUp(array('salsa_event'));
    // Allow anonymous user role to access on salsa event page.
    user_role_change_permissions(DRUPAL_ANONYMOUS_RID, array(
      'view salsa_event' => TRUE,
    ));
  }

  public static function getInfo() {
    return array(
      'name' => 'Salsa Event',
      'description' => 'Tests events',
      'group' => 'Salsa API',
    );
  }

  /**
   * Test method to check subscription phase of an event.
   */
  public function testEventStateSignUp() {
    // Mock the count in order have right values on the confirmation page.
    $count = 1;
    $subscription_count = array(
       1 => array(SALSA_EVENT_SIGNED_UP => $count)
    );
    variable_set('salsa_event_count_attendee_mock', $subscription_count);

    // Check if guest form is around for unlimited events.
    variable_set('salsa_entity_mock_max_attendee', 0);
    $this->drupalGet('salsa/event/1');
    $this->assertFieldByXPath("//input[contains(@class,'guest-action-add')]");

    entity_get_controller('salsa_event')->resetCache(array(1));

    // Check if guest form is around for limited events with at least 2 free
    // seats available.
    variable_set('salsa_entity_mock_max_attendee', 3);
    $this->drupalGet('salsa/event/1');
    $this->assertFieldByXPath("//input[contains(@class,'guest-action-add')]");

    entity_get_controller('salsa_event')->resetCache(array(1));

    // Check if guest form is around for limited events. The maximum is 2 and
    // one attendee is already registered so the guest form should not be dis-
    // played in this case.
    variable_set('salsa_entity_mock_max_attendee', 2);
    $this->drupalGet('salsa/event/1');
    $this->assertNoFieldByXPath("//input[contains(@class,'guest-action-add')]");

    $values = $this->signUpForEvent('Register');
    $event = entity_load_single('salsa_event', 1);
    $replacements = array(
      '@attendees' => $event->Maximum_Attendees,
      '@current' => $count,
    );
    // Check for the right output after sign up completion.
    $text = t('This event is limited to @attendees attendees. @current is currently signed up.', $replacements);
    $this->assertText($text, 'The subscription count is correct');

    // Check if the supporter event was created with the right status.
    $saved = variable_get('salsa_entity_mock_saved', array());
    $this->assertEqual($saved['supporter_event'][999]['_Status'], SALSA_EVENT_SIGNED_UP, format_string('The supporter event was created with the right status: "@status"', array('@status' => SALSA_EVENT_SIGNED_UP)));

    // Check if _Type and event_KEY have the right value.
    $this->assertEqual($saved['supporter_event'][999]['_Type'], 'Supporter', 'Supporter event type is "Supporter');
    $this->assertEqual($saved['supporter_event'][999]['event_KEY'], 1, 'Supporter event reference to the event is 1');

    // Check if supporter key in supporter event is right.
    $this->assertEqual($saved['supporter'][999]['supporter_KEY'], $saved['supporter_event'][999]['supporter_KEY'], 'Supporter key reference in supporter event is correct.');

    $supporter = $saved['supporter'];
    // Checks entered field values match the values of the saved supporter.
    $this->assertEqual($supporter[999]['First_Name'], $values['First_Name'], 'Supporter First name is right.');
    $this->assertEqual($supporter[999]['Last_Name'], $values['Last_Name'], 'Supporter Last name is right.');
    $this->assertEqual($supporter[999]['Email'], $values['Email'], 'Supporter Email is right.');

    entity_get_controller('salsa_event')->resetCache(array(1));

    // Check if form is displayed for unlimited attendees
    variable_set('salsa_entity_mock_max_attendee', 0);
    $this->drupalGet('salsa/event/1');
    $this->assertFieldById('edit-register', t('Register'));
    $this->assertNoText($text, 'No attendee info is displayed on unlimited events.');
    // Check if supporter events get saved with status 'Signed Up'.
    variable_set('salsa_entity_mock_saved', array());
    $this->signUpForEvent('Register');
    $saved = variable_get('salsa_entity_mock_saved', array());
    $this->assertEqual($saved['supporter_event'][999]['_Status'], SALSA_EVENT_SIGNED_UP,
      format_string('The supporter event was created with status "@status" in an event with no limit for attendees.',
        array('@status' => SALSA_EVENT_SIGNED_UP)
      )
    );
  }

  /**
   * Test method to check subscription phase of an event.
   */
  public function testEventStateWaitingList() {
    // Mock the count query to return the expected subscriber count.
    $event = entity_load_single('salsa_event', 1);
    $count = 1;
    $subscription_count = array(
      1 => array(
        SALSA_EVENT_SIGNED_UP => (integer) $event->Maximum_Attendees,
        SALSA_EVENT_WAITING_LIST => 1,
      ),
    );
    variable_set('salsa_event_count_attendee_mock', $subscription_count);

    // Check for the right output after sign up completion.
    $this->signUpForEvent('Attend Waiting List');

    $replacements = array(
      '@count' => $event->Maximum_Waiting_List_Size - $count,
      '@max' => $event->Maximum_Waiting_List_Size,
    );
    $text = t('Currently there is @count of total @max places available on the waiting list.', $replacements);
    $this->assertText($text, 'The subscription count is correct');

    // Check if the supporter event was created with the right status.
    $saved = variable_get('salsa_entity_mock_saved', array());
    $this->assertEqual($saved['supporter_event'][999]['_Status'], SALSA_EVENT_WAITING_LIST, format_string('The supporter event was created with the right status: "@status"', array('@status' => SALSA_EVENT_WAITING_LIST)));
  }

  /**
   * Test method to check subscription phase of an event.
   */
  public function testEventStateFull() {
    // Mock the count query to return the expected subscriber count.
    $event = entity_load_single('salsa_event', 1);
    $subscription_count = array(
      1 => array(
        SALSA_EVENT_SIGNED_UP => (integer) $event->Maximum_Attendees,
        SALSA_EVENT_WAITING_LIST => ((integer) $event->Maximum_Waiting_List_Size) - 1,
      ),
    );
    variable_set('salsa_event_count_attendee_mock', $subscription_count);
    // Subscribe the last remaining seat on the waiting list.
    $this->signUpForEvent('Attend Waiting List');

    $this->assertText(t('This event is already fully booked.'), 'The last seat on the waiting list is used, the event is marked as full now');

    // Check if register button is not available.
    $this->assertNoField('register', 'The register/attend waiting list button is not displayed.');

    // Check if the supporter event was created with the right status.
    $saved = variable_get('salsa_entity_mock_saved', array());
    $this->assertEqual($saved['supporter_event'][999]['_Status'], SALSA_EVENT_WAITING_LIST, format_string('The supporter event was created with the right status: "@status"', array('@status' => SALSA_EVENT_WAITING_LIST)));
  }

  /**
   * Test for mail tokens and required groups on the supporter.
   */
  public function testTokenRequiredGroups() {
    $this->pass('Testing additional mail token data.');
    // Make the event free.
    variable_get('salsa_entity_mock_event_payment', 'false');
    $this->signUpForEvent('Register');

    // Load the saved data with revisions.
    $saved = variable_get('salsa_entity_mock_saved_history', array());
    $event = entity_load_single('salsa_event', 1);

    // Compare the saved values with the supporter properties.
    $this->assertEqual($saved[0]['Event_Name'], $event->Event_Name);
    // Check the values of the saved supporter group (required group).
    $this->assertEqual($saved[1]['supporter_KEY'], 999, 'Supporter groups with reference to the supporter created.');
    $this->assertEqual($saved[1]['groups_KEY'], 1, 'Supporter groups has the right groups key.');
  }

  /**
   * Submits the event sign up form.
   *
   * @param string $submit
   *   Label of the submit button.
   */
  protected function signUpForEvent($submit) {
    // Open the event page.
    $this->drupalGet('salsa/event/1');

    // Submit the supporter information.
    $edit = array(
      'First_Name' => $this->randomName(),
      'Last_Name' => $this->randomName(),
      'Email' =>  $this->randomName() . '@example.org',
    );
    $this->drupalPost(NULL, $edit, t($submit));
    return $edit;
  }
}
