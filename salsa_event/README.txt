
SUMMARY
=======

None.


REQUIREMENTS
============

Required modules:

* salsa_entity
* salsa_donate_page

Optional modules:

* gmap3_tools
    This module is used to display Google map, you need only to download
    the module from URL (http://drupal.org/project/gmap3_tools) and enable it.
    Map will appear automatically if the module is enabled, and if you entered
    a valid address for the event.


INSTALLATION
============

None.


HOW TO USE IT
=============

None.


CONTACT
=======

Current maintainers:

* Sascha Grossenbacher (berdir) - http://drupal.org/user/214652
* Vladimir Mitrovic (devlada) - http://drupal.org/user/1663518
* Sebastian Leu (s_leu) - http://drupal.org/user/1336864